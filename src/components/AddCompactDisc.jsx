import React, { Component } from 'react';
import CdService from '../services/CdService';
import { Redirect } from 'react-router-dom';

export default class AddCompactDisc extends Component {
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeArtist = this.onChangeArtist.bind(this);
    this.onChangePrice = this.onChangePrice.bind(this);
    this.saveCd = this.saveCd.bind(this);

    this.state = {
      title: '',
      artist: '',
      price: '',
      submitted: false,
    };
  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value,
    });
  }

  onChangeArtist(e) {
    this.setState({
      artist: e.target.value,
    });
  }

  onChangePrice(e) {
    this.setState({
      price: e.target.value,
    });
  }

  saveCd() {
    var data = {
      title: this.state.title,
      artist: this.state.artist,
      price: this.state.price,
    };

    if (
      data.title.length < 1 ||
      data.artist.length < 1 ||
      data.price.length < 1
    ) {
      console.log('Log invalid input errors');
      return;
    }

    CdService.add(data)
      .then((response) => {
        this.setState({
          artist: response.data.artist,
          title: response.data.title,
          price: response.data.lname,
          submitted: true,
        });
      })
      .catch((e) => {
        alert(e);
        console.log(e);
      });
  }

  render() {
    if (this.state.submitted) {
      return <Redirect to="CompactDiscs" />;
    }
    return (
      <form>
        <h1>Add A New CD </h1>
        <div className="form-group">
          <label htmlFor="title" className="font-weight-bold">
            Title
          </label>
          <input
            type="text"
            className="form-control"
            id="title"
            aria-describedby="titleHelp"
            placeholder="Title"
            value={this.state.title}
            onChange={this.onChangeTitle}
          />
          <small id="titleHelp" className="form-text text-muted">
            We'll never share your details with anyone else.
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="artist" className="font-weight-bold">
            Artist
          </label>
          <input
            type="text"
            className="form-control"
            id="artist"
            aria-describedby="artistHelp"
            placeholder="Artist"
            value={this.state.artist}
            onChange={this.onChangeArtist}
          />
          <small id="artistHelp" className="form-text text-muted">
            We'll never share your glass with anyone else.
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="price" className="font-weight-bold">
            Price
          </label>
          <input
            type="text"
            className="form-control"
            id="price"
            aria-describedby="priceHelp"
            placeholder="Price"
            value={this.state.price}
            onChange={this.onChangePrice}
          />
          <small id="priceHelp" className="form-text text-muted">
            We'll never share your cheese with anyone else.
          </small>
        </div>

        <div className="form-group">
          <input
            type="button"
            className="form-control btn btn-secondary"
            onClick={this.saveCd}
            value="Submit"
          />
        </div>
      </form>
    );
  }
}
