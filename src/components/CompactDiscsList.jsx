import React, { Component } from 'react';
import CdService from '../services/CdService';

export default class CompactDiscsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cds: [],
      currentIndex: -1,
    };
  }

  async retrieveCds() {
    await CdService.getAll()
      .then((response) => {
        this.setState({
          cds: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  componentDidMount() {
    this.retrieveCds();
  }

  render() {
    const { cds, currentIndex } = this.state;
    return (
      <>
        <div className="container">
          <div className="col-md-12">
            <h1>All Currently CDs Available</h1>

            <ul className="list-group">
              <li className="list-group-item">
                <div className="row">
                  <div className="col-sm font-weight-bold">Artist</div>
                  <div className="col-sm font-weight-bold">Title </div>
                  <div className="col-sm font-weight-bold">Price</div>
                </div>
              </li>
              {cds &&
                cds.map((cd, index) => (
                  <li
                    className={
                      'list-group-item ' +
                      (index === currentIndex ? 'active' : '')
                    }
                    // onClick={() => this.setActiveTutorial(tutorial, index)}
                    key={index}
                  >
                    <div className="row">
                      <div className="col-sm">{cd.artist} </div>
                      <div className="col-sm">{cd.title} </div>
                      <div className="col-sm">{cd.price} </div>
                    </div>
                  </li>
                ))}
            </ul>
          </div>
        </div>
      </>
    );
  }
}
