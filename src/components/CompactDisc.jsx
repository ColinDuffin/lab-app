import React, { Component } from 'react';

import CdService from '../services/CdService';

class CompactDisc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cd: '',
      idToSearch: this.props.match.params.id,
    };
  }
  componentDidMount() {
    this.retrieveCds(this.state.idToSearch);
  }

  async retrieveCds(cdId) {
    await CdService.getById(cdId)
      .then((response) => {
        this.setState({
          cd: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { cd } = this.state;
    return (
      <>
        <h1>The CD You Searched For:</h1>

        <div className="container">
          <div className="row">
            <div className="col-sm font-weight-bold">Artist</div>
            <div className="col-sm font-weight-bold">Title </div>
            <div className="col-sm font-weight-bold">Price</div>
          </div>
          <div className="row">
            <div className="col-sm">{cd.artist}</div>
            <div className="col-sm">{cd.title} </div>
            <div className="col-sm">{cd.price}</div>
          </div>
        </div>
      </>
    );
  }
}

export default CompactDisc;
