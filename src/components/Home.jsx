import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class Home extends Component {
  constructor(props) {
    super(props);
    this.searchId = this.onChangeSearchId.bind(this);

    this.state = {
      searchId: '',
      submitted: false,
    };
  }

  onChangeSearchId = (e) => {
    this.setState({
      searchId: e.target.value,
    });
  };

  searchById = () => {
    this.setState({ submitted: true });
  };

  render() {
    if (this.state.submitted) {
      return <Redirect to={`CompactDiscs/${this.state.searchId}`} />;
    }
    return (
      <>
        <h1>Home</h1>

        <div className="form-group">
          <label htmlFor="search">Search for CD By Stock Number</label>
          <input
            type="text"
            className="form-control"
            id="searchId"
            aria-describedby="searchHelp"
            placeholder=""
            value={this.state.searchId}
            onChange={this.onChangeSearchId}
          />
          <small id="searchHelp" className="form-text text-muted">
            You can look in MongoDB for the _ID associated with each CD record
            and use that to search.
          </small>
        </div>

        <div className="form-group">
          <input
            type="button"
            className="form-control btn btn-secondary"
            onClick={this.searchById}
            value="Submit"
          />
        </div>
      </>
    );
  }
}

export default Home;
