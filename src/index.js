import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';

const myDiv = document.getElementById('root');

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// //This variable contains all the HTML that will be rendered
// let output = <span>Hello there</span>;

// //A DOM element that will contain the entire react generated html
// const myDiv = document.querySelector('#root');
// //OR
// //const myDiv = document.getElementById('root')

// let age = 73;
// const customer = {
//   first_name: 'Bob',
//   last_name: 'Dylan',
// };

// let output = (
//   <span>
//     {' '}
//     {customer.first_name} {customer.last_name} is {age} years old{' '}
//   </span>
// );

// const namesInHTML = [
//   <li key="1">Bob Dust</li>,
//   <li key="2">Fredy Mercury</li>,
//   <li key="4">Shazam Nikola</li>,
//   <li key="44">Wilibin Walabam</li>,
// ];

// const content = <ul>{namesInHTML}</ul>;

// const animals = ['Horse', 'Turtle', 'Elephant', 'Monkey'];
// const animalsList = animals.map((animal) => {
//   return <li key={animal}>{animal}</li>;
// });

// const displayAnimalsList = <ul>{animalsList}</ul>;

// function Greet(props) {
//   return <h1>Hello your name is {props.name}</h1>;
// }

// const greeting = <Greet name="Yeahaboyea" />;

const myApp = (
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

ReactDOM.render(myApp, myDiv);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
