import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
// import logo from './logo.svg';
import './App.css';
import AddCompactDisc from './components/AddCompactDisc';
import CompactDisc from './components/CompactDisc';
import CompactDiscList from './components/CompactDiscsList';
import Home from './components/Home';

function App() {
  return (
    <div className="App">
      <nav className="navbar navbar-expand navbar-dark bg-primary">
        <a href="/Home" className="navbar-brand">
          Home
        </a>

        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={'/CompactDiscs'} className="nav-link">
              CompactDiscs
            </Link>
          </li>
          <li className="nav-item">
            <Link to={'/add'} className="nav-link">
              Add
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={['/', '/Home']} component={Home} />

          <Route
            exact
            path={['/', '/CompactDiscs']}
            component={CompactDiscList}
          />
          <Route exact path="/add" component={AddCompactDisc} />
          <Route path="/CompactDiscs/:id" component={CompactDisc} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
