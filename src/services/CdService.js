import http from '../http-common';

export class CdService {
  getAll() {
    return http.get('/cd/returnAll');
  }

  getTotal() {
    return http.get('/cd/total');
  }

  getById(id) {
    return http.get(`/cd/getById/${id}`);
  }

  add(data) {
    return http.post('/cd/add', data);
  }
}

export default new CdService();
